require("lfs")

function iterPath(rootPath)
	local attr = lfs.attributes(rootPath);
	if attr == nil then
		return
	end

	if attr.mode ~='directory' then
		return
	end
	package.path = package.path..';'..rootPath..'/?.lua'

	local ret, files, iter = pcall(lfs.dir, rootPath);

	if ret ~= false then
		for entry in files, iter do
			local next = false;

			if entry ~='.' and entry~='..' then
				local path = rootPath ..'/'.. entry
				local attr = lfs.attributes(path);
				if attr == nil then
					next = true
				end


				if next == false then
					if attr.mode == 'directory' then
						iterPath(path)
					end
				end

			end
		end
	end
end
