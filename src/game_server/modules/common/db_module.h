#ifndef __MODULES_COMMON_DB_MODULE_H__
#define __MODULES_COMMON_DB_MODULE_H__

#include <boost/asio.hpp>

#include <common/base/module.h>

class Session;

class DbModule : public IModule
{
public:
    virtual bool doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
                            const std::shared_ptr<Player>& player,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);

private:
    bool onCommonRet(const std::shared_ptr<SubServerMgr>& submgr,
                     const std::shared_ptr<Player>& player,
                     const MessageID& msgID,
                     const char* buf,
                     std::size_t bufSize);
};

#endif // __MODULES_COMMON_DB_MODULE_H__
