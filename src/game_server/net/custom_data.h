#ifndef __LOGIN_GATE_NET_CUSTOM_DATA_H__
#define __LOGIN_GATE_NET_CUSTOM_DATA_H__

#include <map>
#include <mutex>

#include <common/net/session.h>

class LgSessionCustomData : public SessionCustomData
{
public:
    LgSessionCustomData(uint32_t playerID, const std::shared_ptr<Session>& dbSession)
        : playerID_(0), dbSession_(dbSession) {}

    virtual void onSessionClose();

    uint32_t getPlayerID() { return playerID_; }

    std::shared_ptr<Session>& getDbSession() { return dbSession_; }
    void setDbSession(const std::shared_ptr<Session>& dbSession) { dbSession_ = dbSession; }
private:
    uint32_t playerID_;
    std::shared_ptr<Session> dbSession_;
};

class DBSessionCustomData : public SessionCustomData
{
public:
    void addLgSession(uint32_t playerID, const std::shared_ptr<Session>& session);
    std::shared_ptr<Session> getLgSession(uint32_t playerID);
    void removeLgSession(uint32_t playerID);

    virtual void onSessionClose();

private:
    std::mutex mutex_;
    std::map<uint64_t, std::weak_ptr<Session>> lgSessions_;
};

template<typename _T>
_T& getCustom(const std::shared_ptr<Session>& session)
{
    auto custom = session->getCustomData();
    if (!custom)
    {
        custom = std::make_shared<_T>();
        session->setCustomData(custom);
    }
    return *std::static_pointer_cast<_T>(custom);
}


#endif // __LOGIN_GATE_NET_CUSTOM_DATA_H__
