#include "custom_data.h"

#include <common/log/log.h>
#include <common/net/session.h>

void LgSessionCustomData::onSessionClose()
{

}

void DBSessionCustomData::onSessionClose()
{
}

void DBSessionCustomData::addLgSession(uint32_t playerID, const std::shared_ptr<Session>& session)
{
    if (!session)
    {
        LOG_ERROR("sessin is null.");
        return;
    }
    std::lock_guard<std::mutex> lock(mutex_);
    lgSessions_[playerID] = session;
}

std::shared_ptr<Session> DBSessionCustomData::getLgSession(uint32_t playerID)
{
    std::lock_guard<std::mutex> lock(mutex_);
    const auto& it = lgSessions_.find(playerID);
    return it == lgSessions_.end() ? std::shared_ptr<Session>() : it->second.lock();
}

void DBSessionCustomData::removeLgSession(uint32_t playerID)
{
    std::lock_guard<std::mutex> lock(mutex_);
    lgSessions_.erase(playerID);
}
