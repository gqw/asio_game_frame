#ifndef _BASE_DB_MGR_H__
#define _BASE_DB_MGR_H__

#include <mysql_driver.h>
#include <mysql_connection.h>

#include <common/base/manager.h>

#include "db_cfg.h"

class DbServerConnection;

/**
 * @desc 数据库连接管理类
 * @auth Qiwei.Gu
 * @date 2015-04-15 13:41:51
 */
class DBMgr : public IManager
{
public:
    DBMgr();
    virtual ~DBMgr();

    bool init();

    /**
     * @desc mysql 数据库的连接
     * @auth Qiwei.Gu
     * @date 2015-05-05 18:03:59
     */
    const std::shared_ptr<sql::Connection>& getMysqlConn() { return mysqlConn_; }

    /**
     * @desc DB Server服务器连接
     * @auth Qiwei.Gu
     * @date 2015-05-05 18:04:19
     */
    DbServerConnection& getDBConn() { return *dbConn_; }

private:
    DBCfg cfg_;
    std::shared_ptr<sql::Connection> mysqlConn_;
    std::shared_ptr<DbServerConnection> dbConn_;

};

#endif // _BASE_DB_MGR_H__
