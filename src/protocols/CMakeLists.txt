cmake_minimum_required(VERSION 2.8)

# 客户端模拟程序
project(proto)
set(subname "protocols")

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/../bin)
set(CMAKE_CXX_FLAGS "-std=c++11 -g3 -O0 -Wall -Wno-deprecated")
include_directories(. "/usr/include/mysql" "/usr/local/include/")
link_directories("/usr/local/lib" "/usr/local/lib/x86_64-linux-gnu/" ${CMAKE_BINARY_DIR}/bin)

# 编译Protobuf文件
find_package(Protobuf REQUIRED)
include_directories(${PROTOBUF_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_BINARY_DIR})

FILE(GLOB_RECURSE PROTO_SRCS "*.proto")
FOREACH(proto_file ${PROTO_SRCS})
  get_filename_component(ABS_FIL ${proto_file} ABSOLUTE)
  get_filename_component(FIL_WE ${ABS_FIL} NAME_WE)
  get_filename_component(ABS_PATH ${ABS_FIL} PATH)

  set(old_bin_dir ${CMAKE_CURRENT_BINARY_DIR})
  set(CMAKE_CURRENT_BINARY_DIR ${ABS_PATH})
  PROTOBUF_GENERATE_CPP(PROTO_SRCS PROTO_HDRS ${ABS_FIL} )
  set(CMAKE_CURRENT_BINARY_DIR ${old_bin_dir})
  list(APPEND src_pc_list ${PROTO_SRCS})
  list(APPEND src_ph_list ${PROTO_HDRS})
ENDFOREACH()

# 生成协议库
add_library(${subname} SHARED ${src_list} ${src_pc_list} )
SET_TARGET_PROPERTIES(${subname} PROPERTIES VERSION 0.0.1 SOVERSION 0)

target_link_libraries(${subname} protobuf)
