// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: db.proto

#ifndef PROTOBUF_db_2eproto__INCLUDED
#define PROTOBUF_db_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2005000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace proto {
namespace db {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_db_2eproto();
void protobuf_AssignDesc_db_2eproto();
void protobuf_ShutdownFile_db_2eproto();

class TableInfo;
class DbReq;
class TableRetInfo;
class DbRet;

enum DbMsgID {
  S2DB_COMMON_REQ = 1,
  DB2S_COMMON_RET = 2,
  S2DB_DEL_REQ = 3,
  DB2S_DEL_RET = 4,
  S2DB_QUERY_REQ = 5,
  DB2S_QUERY_RET = 6,
  S2DB_UPDATE_REQ = 7,
  DB2S_UPDATE_RET = 8,
  S2DB_ADD_REQ = 9,
  DB2S_ADD_RET = 10
};
bool DbMsgID_IsValid(int value);
const DbMsgID DbMsgID_MIN = S2DB_COMMON_REQ;
const DbMsgID DbMsgID_MAX = DB2S_ADD_RET;
const int DbMsgID_ARRAYSIZE = DbMsgID_MAX + 1;

const ::google::protobuf::EnumDescriptor* DbMsgID_descriptor();
inline const ::std::string& DbMsgID_Name(DbMsgID value) {
  return ::google::protobuf::internal::NameOfEnum(
    DbMsgID_descriptor(), value);
}
inline bool DbMsgID_Parse(
    const ::std::string& name, DbMsgID* value) {
  return ::google::protobuf::internal::ParseNamedEnum<DbMsgID>(
    DbMsgID_descriptor(), name, value);
}
enum DbErrCode {
  eFailed = 0,
  eSuc = 1
};
bool DbErrCode_IsValid(int value);
const DbErrCode DbErrCode_MIN = eFailed;
const DbErrCode DbErrCode_MAX = eSuc;
const int DbErrCode_ARRAYSIZE = DbErrCode_MAX + 1;

const ::google::protobuf::EnumDescriptor* DbErrCode_descriptor();
inline const ::std::string& DbErrCode_Name(DbErrCode value) {
  return ::google::protobuf::internal::NameOfEnum(
    DbErrCode_descriptor(), value);
}
inline bool DbErrCode_Parse(
    const ::std::string& name, DbErrCode* value) {
  return ::google::protobuf::internal::ParseNamedEnum<DbErrCode>(
    DbErrCode_descriptor(), name, value);
}
// ===================================================================

class TableInfo : public ::google::protobuf::Message {
 public:
  TableInfo();
  virtual ~TableInfo();

  TableInfo(const TableInfo& from);

  inline TableInfo& operator=(const TableInfo& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const TableInfo& default_instance();

  void Swap(TableInfo* other);

  // implements Message ----------------------------------------------

  TableInfo* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const TableInfo& from);
  void MergeFrom(const TableInfo& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string table_proto_name = 1;
  inline bool has_table_proto_name() const;
  inline void clear_table_proto_name();
  static const int kTableProtoNameFieldNumber = 1;
  inline const ::std::string& table_proto_name() const;
  inline void set_table_proto_name(const ::std::string& value);
  inline void set_table_proto_name(const char* value);
  inline void set_table_proto_name(const char* value, size_t size);
  inline ::std::string* mutable_table_proto_name();
  inline ::std::string* release_table_proto_name();
  inline void set_allocated_table_proto_name(::std::string* table_proto_name);

  // optional bytes table_buffer = 2;
  inline bool has_table_buffer() const;
  inline void clear_table_buffer();
  static const int kTableBufferFieldNumber = 2;
  inline const ::std::string& table_buffer() const;
  inline void set_table_buffer(const ::std::string& value);
  inline void set_table_buffer(const char* value);
  inline void set_table_buffer(const void* value, size_t size);
  inline ::std::string* mutable_table_buffer();
  inline ::std::string* release_table_buffer();
  inline void set_allocated_table_buffer(::std::string* table_buffer);

  // @@protoc_insertion_point(class_scope:proto.db.TableInfo)
 private:
  inline void set_has_table_proto_name();
  inline void clear_has_table_proto_name();
  inline void set_has_table_buffer();
  inline void clear_has_table_buffer();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::std::string* table_proto_name_;
  ::std::string* table_buffer_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(2 + 31) / 32];

  friend void  protobuf_AddDesc_db_2eproto();
  friend void protobuf_AssignDesc_db_2eproto();
  friend void protobuf_ShutdownFile_db_2eproto();

  void InitAsDefaultInstance();
  static TableInfo* default_instance_;
};
// -------------------------------------------------------------------

class DbReq : public ::google::protobuf::Message {
 public:
  DbReq();
  virtual ~DbReq();

  DbReq(const DbReq& from);

  inline DbReq& operator=(const DbReq& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const DbReq& default_instance();

  void Swap(DbReq* other);

  // implements Message ----------------------------------------------

  DbReq* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const DbReq& from);
  void MergeFrom(const DbReq& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional uint32 ret_module_type = 1;
  inline bool has_ret_module_type() const;
  inline void clear_ret_module_type();
  static const int kRetModuleTypeFieldNumber = 1;
  inline ::google::protobuf::uint32 ret_module_type() const;
  inline void set_ret_module_type(::google::protobuf::uint32 value);

  // optional uint32 ret_msg_id = 2;
  inline bool has_ret_msg_id() const;
  inline void clear_ret_msg_id();
  static const int kRetMsgIdFieldNumber = 2;
  inline ::google::protobuf::uint32 ret_msg_id() const;
  inline void set_ret_msg_id(::google::protobuf::uint32 value);

  // repeated .proto.db.TableInfo tables = 3;
  inline int tables_size() const;
  inline void clear_tables();
  static const int kTablesFieldNumber = 3;
  inline const ::proto::db::TableInfo& tables(int index) const;
  inline ::proto::db::TableInfo* mutable_tables(int index);
  inline ::proto::db::TableInfo* add_tables();
  inline const ::google::protobuf::RepeatedPtrField< ::proto::db::TableInfo >&
      tables() const;
  inline ::google::protobuf::RepeatedPtrField< ::proto::db::TableInfo >*
      mutable_tables();

  // @@protoc_insertion_point(class_scope:proto.db.DbReq)
 private:
  inline void set_has_ret_module_type();
  inline void clear_has_ret_module_type();
  inline void set_has_ret_msg_id();
  inline void clear_has_ret_msg_id();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::uint32 ret_module_type_;
  ::google::protobuf::uint32 ret_msg_id_;
  ::google::protobuf::RepeatedPtrField< ::proto::db::TableInfo > tables_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(3 + 31) / 32];

  friend void  protobuf_AddDesc_db_2eproto();
  friend void protobuf_AssignDesc_db_2eproto();
  friend void protobuf_ShutdownFile_db_2eproto();

  void InitAsDefaultInstance();
  static DbReq* default_instance_;
};
// -------------------------------------------------------------------

class TableRetInfo : public ::google::protobuf::Message {
 public:
  TableRetInfo();
  virtual ~TableRetInfo();

  TableRetInfo(const TableRetInfo& from);

  inline TableRetInfo& operator=(const TableRetInfo& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const TableRetInfo& default_instance();

  void Swap(TableRetInfo* other);

  // implements Message ----------------------------------------------

  TableRetInfo* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const TableRetInfo& from);
  void MergeFrom(const TableRetInfo& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string table_proto_name = 1;
  inline bool has_table_proto_name() const;
  inline void clear_table_proto_name();
  static const int kTableProtoNameFieldNumber = 1;
  inline const ::std::string& table_proto_name() const;
  inline void set_table_proto_name(const ::std::string& value);
  inline void set_table_proto_name(const char* value);
  inline void set_table_proto_name(const char* value, size_t size);
  inline ::std::string* mutable_table_proto_name();
  inline ::std::string* release_table_proto_name();
  inline void set_allocated_table_proto_name(::std::string* table_proto_name);

  // repeated bytes proto_buffers = 2;
  inline int proto_buffers_size() const;
  inline void clear_proto_buffers();
  static const int kProtoBuffersFieldNumber = 2;
  inline const ::std::string& proto_buffers(int index) const;
  inline ::std::string* mutable_proto_buffers(int index);
  inline void set_proto_buffers(int index, const ::std::string& value);
  inline void set_proto_buffers(int index, const char* value);
  inline void set_proto_buffers(int index, const void* value, size_t size);
  inline ::std::string* add_proto_buffers();
  inline void add_proto_buffers(const ::std::string& value);
  inline void add_proto_buffers(const char* value);
  inline void add_proto_buffers(const void* value, size_t size);
  inline const ::google::protobuf::RepeatedPtrField< ::std::string>& proto_buffers() const;
  inline ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_proto_buffers();

  // @@protoc_insertion_point(class_scope:proto.db.TableRetInfo)
 private:
  inline void set_has_table_proto_name();
  inline void clear_has_table_proto_name();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::std::string* table_proto_name_;
  ::google::protobuf::RepeatedPtrField< ::std::string> proto_buffers_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(2 + 31) / 32];

  friend void  protobuf_AddDesc_db_2eproto();
  friend void protobuf_AssignDesc_db_2eproto();
  friend void protobuf_ShutdownFile_db_2eproto();

  void InitAsDefaultInstance();
  static TableRetInfo* default_instance_;
};
// -------------------------------------------------------------------

class DbRet : public ::google::protobuf::Message {
 public:
  DbRet();
  virtual ~DbRet();

  DbRet(const DbRet& from);

  inline DbRet& operator=(const DbRet& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const DbRet& default_instance();

  void Swap(DbRet* other);

  // implements Message ----------------------------------------------

  DbRet* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const DbRet& from);
  void MergeFrom(const DbRet& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required uint32 err = 1;
  inline bool has_err() const;
  inline void clear_err();
  static const int kErrFieldNumber = 1;
  inline ::google::protobuf::uint32 err() const;
  inline void set_err(::google::protobuf::uint32 value);

  // repeated .proto.db.TableRetInfo tables = 2;
  inline int tables_size() const;
  inline void clear_tables();
  static const int kTablesFieldNumber = 2;
  inline const ::proto::db::TableRetInfo& tables(int index) const;
  inline ::proto::db::TableRetInfo* mutable_tables(int index);
  inline ::proto::db::TableRetInfo* add_tables();
  inline const ::google::protobuf::RepeatedPtrField< ::proto::db::TableRetInfo >&
      tables() const;
  inline ::google::protobuf::RepeatedPtrField< ::proto::db::TableRetInfo >*
      mutable_tables();

  // @@protoc_insertion_point(class_scope:proto.db.DbRet)
 private:
  inline void set_has_err();
  inline void clear_has_err();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::RepeatedPtrField< ::proto::db::TableRetInfo > tables_;
  ::google::protobuf::uint32 err_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(2 + 31) / 32];

  friend void  protobuf_AddDesc_db_2eproto();
  friend void protobuf_AssignDesc_db_2eproto();
  friend void protobuf_ShutdownFile_db_2eproto();

  void InitAsDefaultInstance();
  static DbRet* default_instance_;
};
// ===================================================================


// ===================================================================

// TableInfo

// required string table_proto_name = 1;
inline bool TableInfo::has_table_proto_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void TableInfo::set_has_table_proto_name() {
  _has_bits_[0] |= 0x00000001u;
}
inline void TableInfo::clear_has_table_proto_name() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void TableInfo::clear_table_proto_name() {
  if (table_proto_name_ != &::google::protobuf::internal::kEmptyString) {
    table_proto_name_->clear();
  }
  clear_has_table_proto_name();
}
inline const ::std::string& TableInfo::table_proto_name() const {
  return *table_proto_name_;
}
inline void TableInfo::set_table_proto_name(const ::std::string& value) {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  table_proto_name_->assign(value);
}
inline void TableInfo::set_table_proto_name(const char* value) {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  table_proto_name_->assign(value);
}
inline void TableInfo::set_table_proto_name(const char* value, size_t size) {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  table_proto_name_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* TableInfo::mutable_table_proto_name() {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  return table_proto_name_;
}
inline ::std::string* TableInfo::release_table_proto_name() {
  clear_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = table_proto_name_;
    table_proto_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void TableInfo::set_allocated_table_proto_name(::std::string* table_proto_name) {
  if (table_proto_name_ != &::google::protobuf::internal::kEmptyString) {
    delete table_proto_name_;
  }
  if (table_proto_name) {
    set_has_table_proto_name();
    table_proto_name_ = table_proto_name;
  } else {
    clear_has_table_proto_name();
    table_proto_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// optional bytes table_buffer = 2;
inline bool TableInfo::has_table_buffer() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void TableInfo::set_has_table_buffer() {
  _has_bits_[0] |= 0x00000002u;
}
inline void TableInfo::clear_has_table_buffer() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void TableInfo::clear_table_buffer() {
  if (table_buffer_ != &::google::protobuf::internal::kEmptyString) {
    table_buffer_->clear();
  }
  clear_has_table_buffer();
}
inline const ::std::string& TableInfo::table_buffer() const {
  return *table_buffer_;
}
inline void TableInfo::set_table_buffer(const ::std::string& value) {
  set_has_table_buffer();
  if (table_buffer_ == &::google::protobuf::internal::kEmptyString) {
    table_buffer_ = new ::std::string;
  }
  table_buffer_->assign(value);
}
inline void TableInfo::set_table_buffer(const char* value) {
  set_has_table_buffer();
  if (table_buffer_ == &::google::protobuf::internal::kEmptyString) {
    table_buffer_ = new ::std::string;
  }
  table_buffer_->assign(value);
}
inline void TableInfo::set_table_buffer(const void* value, size_t size) {
  set_has_table_buffer();
  if (table_buffer_ == &::google::protobuf::internal::kEmptyString) {
    table_buffer_ = new ::std::string;
  }
  table_buffer_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* TableInfo::mutable_table_buffer() {
  set_has_table_buffer();
  if (table_buffer_ == &::google::protobuf::internal::kEmptyString) {
    table_buffer_ = new ::std::string;
  }
  return table_buffer_;
}
inline ::std::string* TableInfo::release_table_buffer() {
  clear_has_table_buffer();
  if (table_buffer_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = table_buffer_;
    table_buffer_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void TableInfo::set_allocated_table_buffer(::std::string* table_buffer) {
  if (table_buffer_ != &::google::protobuf::internal::kEmptyString) {
    delete table_buffer_;
  }
  if (table_buffer) {
    set_has_table_buffer();
    table_buffer_ = table_buffer;
  } else {
    clear_has_table_buffer();
    table_buffer_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// -------------------------------------------------------------------

// DbReq

// optional uint32 ret_module_type = 1;
inline bool DbReq::has_ret_module_type() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void DbReq::set_has_ret_module_type() {
  _has_bits_[0] |= 0x00000001u;
}
inline void DbReq::clear_has_ret_module_type() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void DbReq::clear_ret_module_type() {
  ret_module_type_ = 0u;
  clear_has_ret_module_type();
}
inline ::google::protobuf::uint32 DbReq::ret_module_type() const {
  return ret_module_type_;
}
inline void DbReq::set_ret_module_type(::google::protobuf::uint32 value) {
  set_has_ret_module_type();
  ret_module_type_ = value;
}

// optional uint32 ret_msg_id = 2;
inline bool DbReq::has_ret_msg_id() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void DbReq::set_has_ret_msg_id() {
  _has_bits_[0] |= 0x00000002u;
}
inline void DbReq::clear_has_ret_msg_id() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void DbReq::clear_ret_msg_id() {
  ret_msg_id_ = 0u;
  clear_has_ret_msg_id();
}
inline ::google::protobuf::uint32 DbReq::ret_msg_id() const {
  return ret_msg_id_;
}
inline void DbReq::set_ret_msg_id(::google::protobuf::uint32 value) {
  set_has_ret_msg_id();
  ret_msg_id_ = value;
}

// repeated .proto.db.TableInfo tables = 3;
inline int DbReq::tables_size() const {
  return tables_.size();
}
inline void DbReq::clear_tables() {
  tables_.Clear();
}
inline const ::proto::db::TableInfo& DbReq::tables(int index) const {
  return tables_.Get(index);
}
inline ::proto::db::TableInfo* DbReq::mutable_tables(int index) {
  return tables_.Mutable(index);
}
inline ::proto::db::TableInfo* DbReq::add_tables() {
  return tables_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::proto::db::TableInfo >&
DbReq::tables() const {
  return tables_;
}
inline ::google::protobuf::RepeatedPtrField< ::proto::db::TableInfo >*
DbReq::mutable_tables() {
  return &tables_;
}

// -------------------------------------------------------------------

// TableRetInfo

// required string table_proto_name = 1;
inline bool TableRetInfo::has_table_proto_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void TableRetInfo::set_has_table_proto_name() {
  _has_bits_[0] |= 0x00000001u;
}
inline void TableRetInfo::clear_has_table_proto_name() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void TableRetInfo::clear_table_proto_name() {
  if (table_proto_name_ != &::google::protobuf::internal::kEmptyString) {
    table_proto_name_->clear();
  }
  clear_has_table_proto_name();
}
inline const ::std::string& TableRetInfo::table_proto_name() const {
  return *table_proto_name_;
}
inline void TableRetInfo::set_table_proto_name(const ::std::string& value) {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  table_proto_name_->assign(value);
}
inline void TableRetInfo::set_table_proto_name(const char* value) {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  table_proto_name_->assign(value);
}
inline void TableRetInfo::set_table_proto_name(const char* value, size_t size) {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  table_proto_name_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* TableRetInfo::mutable_table_proto_name() {
  set_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    table_proto_name_ = new ::std::string;
  }
  return table_proto_name_;
}
inline ::std::string* TableRetInfo::release_table_proto_name() {
  clear_has_table_proto_name();
  if (table_proto_name_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = table_proto_name_;
    table_proto_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void TableRetInfo::set_allocated_table_proto_name(::std::string* table_proto_name) {
  if (table_proto_name_ != &::google::protobuf::internal::kEmptyString) {
    delete table_proto_name_;
  }
  if (table_proto_name) {
    set_has_table_proto_name();
    table_proto_name_ = table_proto_name;
  } else {
    clear_has_table_proto_name();
    table_proto_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// repeated bytes proto_buffers = 2;
inline int TableRetInfo::proto_buffers_size() const {
  return proto_buffers_.size();
}
inline void TableRetInfo::clear_proto_buffers() {
  proto_buffers_.Clear();
}
inline const ::std::string& TableRetInfo::proto_buffers(int index) const {
  return proto_buffers_.Get(index);
}
inline ::std::string* TableRetInfo::mutable_proto_buffers(int index) {
  return proto_buffers_.Mutable(index);
}
inline void TableRetInfo::set_proto_buffers(int index, const ::std::string& value) {
  proto_buffers_.Mutable(index)->assign(value);
}
inline void TableRetInfo::set_proto_buffers(int index, const char* value) {
  proto_buffers_.Mutable(index)->assign(value);
}
inline void TableRetInfo::set_proto_buffers(int index, const void* value, size_t size) {
  proto_buffers_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
}
inline ::std::string* TableRetInfo::add_proto_buffers() {
  return proto_buffers_.Add();
}
inline void TableRetInfo::add_proto_buffers(const ::std::string& value) {
  proto_buffers_.Add()->assign(value);
}
inline void TableRetInfo::add_proto_buffers(const char* value) {
  proto_buffers_.Add()->assign(value);
}
inline void TableRetInfo::add_proto_buffers(const void* value, size_t size) {
  proto_buffers_.Add()->assign(reinterpret_cast<const char*>(value), size);
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
TableRetInfo::proto_buffers() const {
  return proto_buffers_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
TableRetInfo::mutable_proto_buffers() {
  return &proto_buffers_;
}

// -------------------------------------------------------------------

// DbRet

// required uint32 err = 1;
inline bool DbRet::has_err() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void DbRet::set_has_err() {
  _has_bits_[0] |= 0x00000001u;
}
inline void DbRet::clear_has_err() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void DbRet::clear_err() {
  err_ = 0u;
  clear_has_err();
}
inline ::google::protobuf::uint32 DbRet::err() const {
  return err_;
}
inline void DbRet::set_err(::google::protobuf::uint32 value) {
  set_has_err();
  err_ = value;
}

// repeated .proto.db.TableRetInfo tables = 2;
inline int DbRet::tables_size() const {
  return tables_.size();
}
inline void DbRet::clear_tables() {
  tables_.Clear();
}
inline const ::proto::db::TableRetInfo& DbRet::tables(int index) const {
  return tables_.Get(index);
}
inline ::proto::db::TableRetInfo* DbRet::mutable_tables(int index) {
  return tables_.Mutable(index);
}
inline ::proto::db::TableRetInfo* DbRet::add_tables() {
  return tables_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::proto::db::TableRetInfo >&
DbRet::tables() const {
  return tables_;
}
inline ::google::protobuf::RepeatedPtrField< ::proto::db::TableRetInfo >*
DbRet::mutable_tables() {
  return &tables_;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace db
}  // namespace proto

#ifndef SWIG
namespace google {
namespace protobuf {

template <>
inline const EnumDescriptor* GetEnumDescriptor< ::proto::db::DbMsgID>() {
  return ::proto::db::DbMsgID_descriptor();
}
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::proto::db::DbErrCode>() {
  return ::proto::db::DbErrCode_descriptor();
}

}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_db_2eproto__INCLUDED
