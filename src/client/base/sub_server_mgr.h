#ifndef __SUB_SERVER_MGR_H__
#define __SUB_SERVER_MGR_H__

#include <common/base/sub_server_mgr.h>

/**
 * @desc 客户端管理器
 * @auth Qiwei.Gu
 * @date 2015-03-25 23:29:57
 */
class SubServerMgr : public ISubServerMgr
{
public:
    /**
     * @desc 构造函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:30:12
     */
    SubServerMgr();

    /**
     * @desc 析构函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:30:36
     */
    virtual ~SubServerMgr() {}

    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:30:54
     */
    virtual bool init();

     /**
     * @desc 查询管理器类
     * @auth Qiwei.Gu
     * @date 2015-03-21 12:57:13
     */
    virtual std::shared_ptr<IManager> getSubMgr(SubManagerType type);

private:
    /**
     * @desc 功能模块初始化
     * @auth Qiwei.Gu
     * @date 2015-04-05 22:04:58
     */
    bool initModule();

    /**
     * @desc 命令模块出现化
     * @auth Qiwei.Gu
     * @date 2015-04-05 22:05:42
     */
    bool initCmdModule();

private:
    std::shared_ptr<Session> loginGateSession_;
};

#endif // __SUB_SERVER_MGR_H__
