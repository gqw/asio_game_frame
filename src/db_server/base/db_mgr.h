#ifndef _BASE_DB_MGR_H__
#define _BASE_DB_MGR_H__

#include <mutex>

#include <mysql_driver.h>
#include <mysql_connection.h>

#include <common/base/manager.h>

class DBCfg;

/**
 * @desc 数据库连接管理类
 * @auth Qiwei.Gu
 * @date 2015-04-15 13:41:51
 */
class DBMgr : public IManager
{
public:
    DBMgr();
    virtual ~DBMgr();

    bool init();

    const std::shared_ptr<sql::Connection>& getMysqlConn() { return mysqlConn_; }

    bool executeSql(const std::string& sqls);
    bool executeSql(const std::vector<std::string>& sqls);

    /* bool queryTable(const proto::db::TableInfo& tableInfo, */
    /*                 const google::protobuf::Message *msg, */
    /*                 const google::protobuf::Descriptor *des, */
    /*                 TableRetInfo& retInfo); */

private:
    std::mutex mutex_;
    std::shared_ptr<DBCfg> cfg_;
    std::shared_ptr<sql::Connection> mysqlConn_;

};

#endif // _BASE_DB_MGR_H__
