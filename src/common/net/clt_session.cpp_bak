#include "clt_session.h"

#include <stdint.h>

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <proto/login/login.pb.h>

#include "common/base/module.h"
#include "common/base/module_mgr.h"
#include "common/base/server_mgr.h"
#include "common/base/sub_server_mgr.h"
#include "common/log/log.h"
#include "common/net/message_parse.h"
#include "common/net/session.h"
#include "common/net/session_mgr.h"
#include "common/utils/util.h"

#include "login_gate/base/account_mgr.h"
#include "login_gate/base/sub_server_mgr.h"

CltSession::CltSession(boost::asio::io_service& ios,
                       const std::shared_ptr<SessionMgr>& sessionMgr,
                       const std::shared_ptr<ServerNetInfo>& netInfo,
                       SessionCreateType createType)
    : Session(ios, sessionMgr, netInfo, createType)
{
    setCustomData(std::make_ptr<CltSessionCustomData>());
}

std::shared_ptr<Session> CltSession::chooseAGsSession()
{
    auto sessionMgr = sessionMgr_.lock();
    if (!sessionMgr)
    {
        LOG_ERROR("Session manager not found.");
        return std::shared_ptr<Session>();
    }
    std::vector<std::shared_ptr<Session> > sessions;
    sessionMgr->getConnSessions(ServerType::eGameServer, SessionType::eNormal, sessions);

    if (sessions.empty())
    {
        LOG_ERROR("Game server not config.");
        return std::shared_ptr<Session>();
    }

    auto session = sessions[++lastGsSesionIndex_ % sessions.size()];
    if (!session)
    {
        LOG_ERROR("session not found.");
        return session;
    }

    auto gsSession = ServerMgr::get().getSubServ()->createSession(SessionType::eNormal,
                                                             sessionMgr,
                                                             session->getNetInfo(),
                                                             eSessionCreateConn);
    if (!gsSession)
    {
        LOG_ERROR("create game server session failed.");
        return conn;
    }

    if (!gsSession->initConn(false))
    {
        LOG_ERROR("Connect to game server failed. to id:" << std::hex << session->getNetInfo()->getID());
        return std::shared_ptr<Session>();
    }
    auto gsCustom = std::make_ptr<GsSessionCustomData>();
    gsCustom->cltSession = shared_from_this();
    gsSession->setCustomData(gsCustom);
    return gsSession;
}

/**
 * @desc 处理读取的数据
 * @auth Qiwei.Gu
 * @date 2015-03-21 17:50:57
 */
void CltSession::handleRead(const boost::system::error_code& e,
                            std::size_t bytes_transferred)
{
    ParseResultState result = ParseResultState::eFailed;
    std::vector<char> tmpBuf(bytes_transferred);

    if (e && e != boost::asio::error::operation_aborted)
    {
        goto failed;
    }

    if (getCltCustom().gsSession_ && getCltCutsom().playerID_ != -1)
    {
        // 已经登录
        const auto& buf = msgParse_->getConstBufs();
        gsSession_->sendData(buffer_cast<const char*>(buf.data()),
                              buffer_size(buf.data()));
        return startRead();
    }
    memcpy(&tmpBuf[0], buffer_cast<const char*>(msgParse_->getConstBufs().data()), bytes_transferred);
    result = msgParse_->parse(bytes_transferred);
    if (result == ParseResultState::eSuc)
    {
        auto& header = msgParse_->getMsgHeader();
        if (header.msgID.stMsg.moduleType != ModuleType::eLogin)
        {
            return startRead();
        }
        if (header.msgID.stMsg.msgID != proto::login::C_S_LOGIN &&
            header.msgID.stMsg.msgID != proto::login::C_S_LOGIN_CONFIRM)
        {
            return startRead();
        }

        if (header.msgID.stMsg.msgID == proto::login::C_S_LOGIN_CONFIRM)
        {
#pragma TODO_WARN( "gqw", "关闭其他登录")
            return startRead();
        }

        auto& bufs = msgParse_->getConstBufs();
        proto::login::LoginReq req;
        if (!req.ParseFromArray(buffer_cast<const char*>(bufs.data()),
                                buffer_size(bufs.data())))
        {
            LOG_ERROR("Parse proto data failed." << std::hex << header.msgID.msgID);
            return startRead();
        }

        auto accMgr = ServerMgr::get().getSubServ<SubServerMgr>()->getSubMgr<AccountMgr>(SubManagerType::eAccount);
        if (!accMgr)
        {
            LOG_ERROR("No Account manager was init");
            return startRead();
        }
        if (accMgr->hasAccount(req.acc_id()))
        {
#pragma TODO_WARN( "gqw", "应该发送消息给客户端确认，是否强制用户退出以前的登录")
            return startRead();
        }

        if (!getCustom().gsSession)
        {
            getCustom().gsSession = chooseAGsSession();
            if (!getCustom().gsSession)
            {
                LOG_ERROR("Create Game Server session failed.");
                return startRead();
            }
        }

        const auto& buf = msgParse_->getConstBufs();
        gsSession_->sendData(&tmpBuf[0], bytes_transferred);
        return startRead();
    }
    else if (result == ParseResultState::eNeedMore)
    {
        return startRead();
    }
    else
    {
        goto failed;
    }
    return;

failed:
    auto sessionMgr = sessionMgr_.lock();
    if (!sessionMgr)
    {
        return;
    }
    sessionMgr->removeSession(shared_from_this());
    LOG_ERROR(e.category().name() << "(" << e.value() << "):" << e.message());

    if (getCreateType() == eSessionCreateConn)
    {
        reconnTimer_.expires_from_now(boost::posix_time::millisec(netInfo_->reconnInterval));
        reconnTimer_.async_wait(std::bind(&Session::initConn, this, true));
        return;
    }
    return;
}


bool CltSession::dispatchMessage(const MessageID& msgID, const char* buf, std::size_t bufSize)
{
    auto moduleMgr = ServerMgr::get().getMgr<ModuleMgr>(ManagerType::eModule);
    if (!moduleMgr)
    {
        LOG_ERROR("No found module manager.");
        return false;
    }
    auto module = moduleMgr->getModule(msgID.stMsg.moduleType);
    if (!module)
    {
        LOG_ERROR("No found module " << (uint16_t)msgID.stMsg.moduleType) ;
        return false;
    }
    LOG_DEBUG("Recv msg id: " << std::hex << msgID.msgID << std::dec << " size:" << bufSize);
    return module->doDispatch(shared_from_this(), msgID, buf, bufSize);
}
