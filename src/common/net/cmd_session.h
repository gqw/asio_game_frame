#ifndef __COMMON_NET_CMD_SESSION_H__
#define __COMMON_NET_CMD_SESSION_H__

#include <boost/asio.hpp>

#include "session.h"

class SessionMgr;

class CmdSession : public Session
{
public:
    explicit CmdSession(boost::asio::io_service& ios,
                        const std::shared_ptr<SessionMgr>& sessionMgr,
                        const std::shared_ptr<ServerNetInfo>& netInfo,
                        SessionCreateType createType);

    virtual ~CmdSession() {};

    std::shared_ptr<CmdSession> getSharedPtr();

protected:
    /**
     * @desc 开始处理网络事件
     * @auth Qiwei.Gu
     * @date 2015-03-21 17:48:03
     */
    virtual void startRead();

    /**
     * @desc 处理读取的数据
     * @auth Qiwei.Gu
     * @date 2015-03-21 17:50:57
     */
    virtual void handleRead(const boost::system::error_code& e,
                            std::size_t bytes_transferred);


private:
    boost::asio::streambuf bufs_;
};

#endif //  __COMMON_NET_CMD_SESSION_H__
