#ifndef __COMMON_UTILS_UTIL_H__
#define __COMMON_UTILS_UTIL_H__

#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <memory>

#include <boost/format.hpp>

#include <google/protobuf/message.h>
#include <google/protobuf/descriptor.h>

#include <mysql_connection.h>
#include <cppconn/resultset.h>

#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define TODO_WARN(adder, msg) message( __FILE__ "("__STR1__(__LINE__)") : Warning : [" adder "], " msg)
#define __test message\("aaaaaaaaaaaaa"\)



/**
 * @desc 获得当前秒数
 * @auth Qiwei.Gu
 * @date 2015-05-04 15:56:58
 */
time_t getCurrentSecond();

/**
 * @desc 64为数字大头小头转换
 * @auth Qiwei.Gu
 * @date 2015-05-04 15:57:22
 */
uint64_t hton64(uint64_t n);
uint64_t ntoh64(uint64_t n);

// 字符串格式化
template<class TFirst>
void string_format(boost::format& fmt, TFirst&& first)
{
    fmt % first;
}

template<class TFirst, class... TOther>
void string_format(boost::format& fmt, TFirst&& first, TOther&&... other)
{
    fmt % first;
    string_format(fmt, other...);
}

template<class TFirst, class... TOther>
std::string string_format(const char* format, TFirst&& first, TOther&&... other)
{
    boost::format fmt(format);
    string_format(fmt, first, other...);
    return fmt.str();
}

/**
 * @desc 数据库操作函数
 * @auth Qiwei.Gu
 * @date 2015-05-04 15:58:08
 */
bool isSupportFieldType(const google::protobuf::Reflection* ref,
                        const google::protobuf::Message* msg,
                        const google::protobuf::FieldDescriptor* field);

std::string getFieldAsString(const google::protobuf::Reflection* ref,
                             const google::protobuf::Message* msg,
                             const google::protobuf::FieldDescriptor* field);

bool setProtoField(const google::protobuf::Reflection* ref,
                   google::protobuf::Message* msg,
                   const google::protobuf::FieldDescriptor* field,
                   const std::unique_ptr<sql::ResultSet>& rst);

bool initProtoDataFromDb(const std::shared_ptr<sql::Connection>& conn, google::protobuf::Message* msg);


#endif //  __COMMON_UTILS_UTIL_H__
