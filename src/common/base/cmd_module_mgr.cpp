#include "cmd_module_mgr.h"

#include "common/base/cmd_module.h"
#include "log/log.h"

bool CmdModuleMgr::init()
{
    return true;
}

bool CmdModuleMgr::addModule(const std::string& moduleName, std::shared_ptr<ICmdModule> module)
{
    if (!module)
    {
        LOG_ERROR("Add cmd module failed, module is null. module type: " << moduleName) ;
        return false;
    }
    modules_[moduleName] = module;
    LOG_INFO("Add module suc, module type is " <<  moduleName) ;
    return true;
}

std::shared_ptr<ICmdModule> CmdModuleMgr::getModule(const std::string& moduleName)
{
    auto it = modules_.find(moduleName);
    return it == modules_.end() ? std::shared_ptr<ICmdModule>() : it->second;
}
