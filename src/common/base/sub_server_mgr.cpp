#include "sub_server_mgr.h"

#include "common/base/server_cfg.h"
#include "common/log/log.h"
#include "common/net/cmd_session.h"
#include "common/net/session.h"
#include "common/net/session_mgr.h"

/**
 * @desc 工厂方法，根据类型创建连接会话
 * @auth Qiwei.Gu
 * @date 2015-04-04 23:18:20
 */
std::shared_ptr<Session> ISubServerMgr::createSession(SessionType sessionType,
                                                     const std::shared_ptr<SessionMgr>& sessionMgr,
                                                     const std::shared_ptr<ServerNetInfo>& info,
                                                     SessionCreateType createType)
{
    switch(sessionType)
    {
    case SessionType::eNormal:
        return std::make_shared<Session>(ServerMgr::get().getIOS(), sessionMgr, info, createType);

    case SessionType::eCmd:
        return std::make_shared<CmdSession>(ServerMgr::get().getIOS(), sessionMgr, info, createType);

    default:
        LOG_ERROR("SessionType " << uint16_t(sessionType) << " not found, when create session");
        return std::shared_ptr<Session>();
    }
}
