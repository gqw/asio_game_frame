#ifndef __COMMON_SUB_SERVER_MGR_H__
#define __COMMON_SUB_SERVER_MGR_H__

#include "server_mgr.h"

class Session;
class SessionMgr;
struct ServerNetInfo;

enum SessionType : uint16_t;
enum SessionCreateType : uint16_t;
enum SubManagerType : uint16_t;

/**
 * 具体服务管理类接口
 * @date 2015-03-25 22:51:50
 */
class ISubServerMgr
{
public:
    /**
     * @desc 构造函数
     * @param serverType 服务器类型
     * @auth Qiwei.Gu
     * @date 2015-03-25 22:57:03
     */
    ISubServerMgr(ServerType serverType) : serverType_(serverType) {}

    /**
     * @desc 活得服务器类型
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:01:07
     */
    ServerType getServerType() { return serverType_; }

    /**
     * @desc 析构函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 22:58:23
     */
    virtual ~ISubServerMgr() {}

    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-03-25 22:58:36
     */
    virtual bool init() = 0;

     /**
     * @desc 查询管理器类
     * @auth Qiwei.Gu
     * @date 2015-03-21 12:57:13
     */
    virtual std::shared_ptr<IManager> getSubMgr(SubManagerType type) = 0;


    /**
     * @desc 工厂方法，根据类型创建连接会话
     * @auth Qiwei.Gu
     * @date 2015-04-04 23:18:20
     */
    virtual std::shared_ptr<Session> createSession(SessionType sessionType,
                                                   const std::shared_ptr<SessionMgr>& sessionMgr,
                                                   const std::shared_ptr<ServerNetInfo>& info,
                                                   SessionCreateType createType);

protected:
    ServerType serverType_;     // 服务器类型
};

#endif //__COMMON_SUB_SERVER_MGR_H__
