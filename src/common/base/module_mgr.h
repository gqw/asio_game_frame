#ifndef __MODULE_MGR_H__
#define __MODULE_MGR_H__

#include <map>
#include <memory>

#include "manager.h"

class IModule;

class ModuleMgr : public IManager, public std::enable_shared_from_this<ModuleMgr>
{
public:
    virtual ~ModuleMgr() {}

    bool init();

    bool addModule(uint16_t type, std::shared_ptr<IModule> module);

    std::shared_ptr<IModule> getModule(uint16_t type);

    template<typename T>
    std::shared_ptr<T> getModule(uint16_t type)
    {
        auto module = getModule(type);
        return module ? std::dynamic_pointer_cast<T>(module) : std::shared_ptr<T>();
    }

private:
    std::map<uint16_t, std::shared_ptr<IModule>> modules_;
};

#endif // __MODULE_MGR_H__
