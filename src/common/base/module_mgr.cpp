#include "module_mgr.h"

#include "module.h"
#include "log/log.h"

bool ModuleMgr::init()
{
    return true;
}

bool ModuleMgr::addModule(uint16_t type, std::shared_ptr<IModule> module)
{
    if (!module)
    {
        LOG_ERROR("Add module failed, module is null. module type: " << (int)type) ;
        return false;
    }
    modules_[type] = module;
    LOG_INFO("Add module suc, module type is " <<  (int)type) ;
    return true;
}

std::shared_ptr<IModule> ModuleMgr::getModule(uint16_t type)
{
    auto it = modules_.find(type);
    return it == modules_.end() ? std::shared_ptr<IModule>() : it->second;
}
