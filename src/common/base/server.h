#ifndef __COMMON_SERVER_H__
#define __COMMON_SERVER_H__

#include <memory>

#include <boost/asio.hpp>

#include <google/protobuf/message.h>

union MessageID;

class IServer
{
public:
	virtual ~IServer() {}

	virtual bool send(MessageID msgID, uint32_t sessionID, const google::protobuf::Message& msg) = 0;
};

#endif // __COMMON_SERVER_H__
