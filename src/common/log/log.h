#ifndef __LOG_LOG_H__
#define __LOG_LOG_H__

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/helpers/loglog.h>

/**
 * @desc 初始化日志系统
 * @auth Qiwei.Gu
 * @date 2015-03-25 23:38:48
 */
bool initLog(const std::string& logCfgFile);

/**
 * @desc 初始化日志系统
 * @param 日志配置文件路径
 * @auth Qiwei.Gu
 * @date 2015-03-25 23:38:48
 */
bool loadLogCfgFile(const std::string& logCfgFile);

extern log4cplus::Logger g_rootLogger;
#define LOG_DEBUG(params)         LOG4CPLUS_DEBUG(g_rootLogger, params)
#define LOG_INFO(params)          LOG4CPLUS_INFO(g_rootLogger, params)
#define LOG_WARN(params)          LOG4CPLUS_WARN(g_rootLogger, params)
#define LOG_ERROR(params)         LOG4CPLUS_ERROR(g_rootLogger, params)

#endif // __LOG_LOG_H__
