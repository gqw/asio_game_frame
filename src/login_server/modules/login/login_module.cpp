#include "login_module.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <common/log/log.h>
#include <common/net/message_parse.h>
#include <common/net/session.h>

#include <proto/login/login.pb.h>
using namespace proto::login;

bool LoginModule::doDispatch(const std::shared_ptr<Session>& session,
                             const MessageID& msgID,
                             const char* buf,
                             std::size_t bufSize)
{
    switch (msgID.stMsg.msgID)
    {
    case C_S_LOGIN:
        return onLogin(session, msgID, buf, bufSize);

    default:
        return false;
    }

    return false;
}

bool LoginModule::onLogin(const std::shared_ptr<Session>& session,
                          const MessageID& msgID,
                          const char* buf,
                          std::size_t bufSize)
{
    LoginReq req;
    if (!req.ParseFromArray(buf, bufSize))
    {
        LOG_ERROR("Parse proto data failed.");
        return false;
    }
    LOG_DEBUG("Got login req, acc_name" << req.acc_name());


    return true;
}
