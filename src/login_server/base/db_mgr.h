#ifndef _BASE_DB_MGR_H__
#define _BASE_DB_MGR_H__

#include <mysql_driver.h>
#include <mysql_connection.h>

#include <common/base/manager.h>
#include "db_cfg.h"

/**
 * @desc 数据库连接管理类
 * @auth Qiwei.Gu
 * @date 2015-04-15 13:41:51
 */
class DBMgr : public IManager
{
public:
    virtual ~DBMgr();

    bool init();

    const std::shared_ptr<sql::Connection>& getMysqlConn() { return conn_; }

private:
    DBCfg cfg_;
    std::shared_ptr<sql::Connection> conn_;

};

#endif // _BASE_DB_MGR_H__
