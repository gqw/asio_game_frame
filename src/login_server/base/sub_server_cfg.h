#ifndef __BASE_SUB_SERVER_CFG_H__
#define __BASE_SUB_SERVER_CFG_H__


#include <common/base/config.h>

/**
 * @desc 具体服务类配置类
 * @auth Qiwei.Gu
 * @date 2015-04-15 14:39:17
 */
class SubServerCfg : public IConfig
{
public:
    virtual bool init();

    virtual std::shared_ptr<IConfig> makeNewObject()
    {
        return std::make_shared<SubServerCfg>();
    }
private:

};


#endif // __BASE_SUB_SERVER_CFG_H__
