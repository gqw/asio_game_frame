project(gtest CXX C)

cmake_minimum_required(VERSION 2.8)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)
include_directories(. "/usr/include/mysql" "/usr/include/log4cplus" "/usr/include/gtest")

aux_source_directory(. src_list)
add_executable(login_gate_test ${src_list})

target_link_libraries(login_gate_test gtest gtest_main pthread)
