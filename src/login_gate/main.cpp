#include <iostream>

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <log4cplus/loggingmacros.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>
using namespace log4cplus;
using namespace log4cplus::helpers;

#include <common/base/server_mgr.h>

#include "login_gate/base/sub_server_mgr.h"

#include <google/protobuf/descriptor.h>
using namespace google::protobuf;

#include <protocols/protos/login.pb.h>
using namespace proto::login;

// #include <iostream>
// #include <sstream>

// int i = 10;

// int& get()
// {
//     return i;
// }

int main()
{
    LoginReq req;
    req.set_acc_name("test");
    const google::protobuf::Message& msg = req;
    const auto& desc = msg.GetDescriptor();
    std::string type_name = desc->full_name();
    std::string name = desc->name();
    std::cout << type_name << "   " << name << std::endl;
    std::cout << msg.DebugString() << std::endl;
    // std::cout << i << std::endl;
    // int& b = get();
    // b+=10;
    // std::cout << b << std::endl;
    // auto& c = get();
    // c+=20;
    // std::cout << c << std::endl;
    // return 0;
    // proto::login::LoginReq req;
    // req.set_acc_name("test");
    // std::cout << req.has_acc_name() << std::endl;
    // std::stringstream os;
    // req.SerializeToOstream(&os);

    // std::string type_name = proto::login::LoginReq::descriptor()->full_name();
    // const Descriptor* descriptor = DescriptorPool::generated_pool()->FindMessageTypeByName("proto.login.LoginReq");
    // const Message* prototype = MessageFactory::generated_factory()->GetPrototype(descriptor);
    // proto::login::LoginReq* ret = dynamic_cast<proto::login::LoginReq* >(prototype->New());
    // ret->ParseFromIstream(&os);
    // std::cout << ret->acc_name() << std::endl;


    if (!ServerMgr::get().init(std::make_shared<SubServerMgr>()))
    {
        std::cerr << "Init Server manager failed." << std::endl;
        return 1;
    }
    return 0;
}
