#include "player.h"

#include <common/log/log.h>
#include <common/net/message_parse.h>
#include <common/net/session.h>
#include <common/utils/util.h>

#include <common.pb.h>
#include <tables.pb.h>

#include "base/sub_server_mgr.h"
#include "base/db_mgr.h"

Player::Player(uint32_t playerID)
    : playerID_(playerID)
{
}


bool Player::init()
{
    return true;
}


bool Player::sendMsg(const MessageID& msgID, google::protobuf::Message& msg)
{
    if (!gsSession_)
    {
        LOG_ERROR("Player send msg, but lg session is null. msg id:" << std::hex << msgID.msgID );
        return false;
    }
    proto::common::CommonGateRet ret;
    ret.set_player_id(getID());
    ret.set_msg_id(msgID.msgID);
    std::string buf = msg.SerializeAsString();
    ret.set_msg_len(buf.size());
    ret.set_msg(std::move(buf));
    gsSession_->sendMsg(proto::common::eCommon, proto::common::S2C_COMMON_GATE_RET, ret);
    return true;
}

bool Player::sendMsg(uint16_t moduleType, uint16_t msgID, google::protobuf::Message& msg)
{
    if (!gsSession_)
    {
        LOG_ERROR("login gate session not set.");
        return false;
    }
    return gsSession_->sendMsg(MessageID(moduleType, msgID), msg);
}
