#ifndef __LOGIN_GATE_NET_CUSTOM_DATA_H__
#define __LOGIN_GATE_NET_CUSTOM_DATA_H__

#include <map>
#include <mutex>

#include <common/net/session.h>

class Player;

class CltSessionCustomData : public SessionCustomData
{
public:
    const std::shared_ptr<Player>& getPlayer() const { return player_; }
    void setPlayer(const std::shared_ptr<Player>& player) { player_ = player; }

    uint32_t getPlayerID();

    virtual void onSessionClose();

private:
    std::shared_ptr<Player> player_;

/* public: */
/*     std::shared_ptr<Session> gsSession; */
/*     uint32_t playerID; */

/*     CltSessionCustomData() : playerID(0) {} */
};

/* class GsSessionCustomData : public SessionCustomData */
/* { */
/* public: */
/*     void addCltSession(uint32_t playerID, const std::shared_ptr<Session>& session); */
/*     std::shared_ptr<Session> getCltSession(uint32_t playerID); */
/*     void removeCltSession(uint32_t playerID); */

/*     virtual void onSessionClose(); */

/* private: */
/*     std::mutex mutex_; */
/*     std::map<uint64_t, std::weak_ptr<Session>> cltSessions_; */
/* }; */

template<typename _T>
_T& getCustom(const std::shared_ptr<Session>& session)
{
    auto custom = session->getCustomData();
    if (!custom)
    {
        custom = std::make_shared<_T>();
        session->setCustomData(custom);
    }
    return *std::static_pointer_cast<_T>(custom);
}


#endif // __LOGIN_GATE_NET_CUSTOM_DATA_H__
