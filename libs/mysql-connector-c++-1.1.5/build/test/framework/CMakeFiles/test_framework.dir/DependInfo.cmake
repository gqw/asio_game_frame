# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/common/file.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/__/common/file.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/common/stringutils.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/__/common/stringutils.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/start_options.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/start_options.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_asserts.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_asserts.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_container.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_container.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_factory.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_factory.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_filter.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_filter.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_listener.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_listener.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_runner.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_runner.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_suite.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_suite.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_tapOutputter.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_tapOutputter.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/framework/test_timer.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/test_timer.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DBUG_OFF"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/mysql"
  "."
  "cppconn"
  "driver/nativeapi"
  ".."
  "../cppconn"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
